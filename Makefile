ASM = nasm
ASMFLAGS = -f elf64 -g
LD = ld
PYTHON = python3
target = ./target
src = ./src
INC_FILES = $(wildcard $(SRC_DIR)/*.inc)

$(target)/%.o: $(src)/%.asm $(INC_FILES)
	$(ASM) $(ASMFLAGS) -o $@ $<
main: $(target)/lib.o $(target)/main.o $(target)/dict.o 
	$(LD) -o $(target)/$@ $^

.PHONY: clean run test
clean:
	rm -rf $(target)/*.o main

run:
	$(target)/main

test: main
	$(PYTHON) ./test.py
