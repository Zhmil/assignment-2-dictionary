%include "./src/lib.inc"

%define WORD_SIZE 8

global find_word


section .text

find_word:
	push rbx
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi

	test r13, r13	; dict is empty
	jz .not_found

	.loop:
		mov rbx, qword[r13 + WORD_SIZE * 2] ; value (string pointer)
		mov rdi, qword[r13 + WORD_SIZE] ; key (string pointer)
		mov rsi, r12
		
		call string_equals
		
		cmp rax, 1
		je .found
		mov r13, qword[r13] ; pointer to next node in dict
		test r13, r13
		jz .not_found
		jmp .loop		
	.not_found:
		xor rax, rax
		jmp .restore
	.found:
		mov rax, rbx
	.restore:
		pop r13
		pop r12
		pop rbx
		ret
	
