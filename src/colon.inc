%macro colon 2
        %ifndef dict_start
            %define dict_start 0
        %endif

        %2:
            dq dict_start
            dq %%key
            dq %%value

        %define dict_start %2

        %%key: db %1, 0
        %%value:
%endmacro

