%include "./src/dict.inc"
%include "./src/lib.inc"
%include "./src/words.inc"


%define MAX_KEY_LENGTH 255
%define ERROR_EXIT_CODE 1


section .rodata:
error_msg_not_found: db "Word not found", 0
error_msg_too_long: db "Line is too long", 0


section .text
global _start

_start:
	sub rsp, MAX_KEY_LENGTH + 1
	
	mov rdi, rsp
	mov rsi, MAX_KEY_LENGTH
	call read_line
	mov rdi, error_msg_too_long
	test rax, rax
	jz .error

	mov rdi, rax
	mov rsi, dict_start	; dict_start - address of first node of dict	
	call find_word
	mov rdi, error_msg_not_found
	test rax, rax
	jz .error

	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	jmp exit	

	.error:
		call print_error
		call print_newline
		mov rdi, ERROR_EXIT_CODE
		jmp exit
	

	
	
	
