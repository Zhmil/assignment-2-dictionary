# !/usr/bin/python3
import unittest
from subprocess import CalledProcessError, Popen, PIPE
from typing import Tuple


class Test(unittest.TestCase):
    FILENAME = 'main'
    EXIT_CODE_ERROR = 1
    EXIT_CODE_OK = 0
    MAX_KEY_LENGTH = 255
    TARGET_DIR = './target'


    def launch(self, input_string) -> Tuple[str, str, int]:
        p = Popen([f'{Test.TARGET_DIR}/{Test.FILENAME}'], stderr=PIPE, stdin=PIPE, stdout=PIPE, text=True)
        stdout, stderr = p.communicate(input_string)
        return stdout.strip(), stderr.strip(), p.returncode

    def test_not_found(self):
        lines = ['asdkbasdka', 'qwe qweqe qe']
        for line in lines:
            (out, err, code) = self.launch(line)
            self.assertEqual(code, Test.EXIT_CODE_ERROR)
            self.assertEqual(err, "Word not found")
            self.assertEqual(out, "")

    def test_too_long(self):
        lines = ['a' * (Test.MAX_KEY_LENGTH + 5), 'v v v' * (Test.MAX_KEY_LENGTH + 5)]
        for line in lines:
            (out, err, code) = self.launch(line)
            self.assertEqual(code, Test.EXIT_CODE_ERROR)
            self.assertEqual(err, "Line is too long")
            self.assertEqual(out, "")

    def test_success(self):
        input_data = ['first word',
                      'second word',
                      'third word']
        output_data = ['first word explanation',
                       'second word explanation',
                       'third word explanation']
        for (in_, out_) in zip(input_data, output_data):
            (out, err, code) = self.launch(in_)
            self.assertEqual(code, Test.EXIT_CODE_OK)
            self.assertEqual(err, "")
            self.assertEqual(out, out_)


if __name__ == "__main__":
    unittest.main(failfast=False, buffer=False)
